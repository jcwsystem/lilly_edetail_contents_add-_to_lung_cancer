var xmlHttp = createHttpRequest();
var logid = "0";
var interval = 10000;     //ログ取得間隔（初回用）　２回目以降はweb.configの設定による
var timer;                //タイマー用
var cookieNameTemplate = "movieValidity"; //閲覧許可チケットクッキー名テンプレ 

if (xmlHttp != null) {
  callLiveLog();
  timer = window.setInterval('callLiveLog()', interval);
}

//XMLHttpRequestオブジェクト生成
function createHttpRequest() {
  //Win ie用
  if (window.ActiveXObject) {
    try {
      //MSXML2以降用
      return new ActiveXObject("Msxml2.XMLHTTP");
    } catch (e) {
      try {
        //旧MSXML用
        return new ActiveXObject("Microsoft.XMLHTTP");
      } catch (e2) {
        return null;
      }
    }
  } else if (window.XMLHttpRequest) {
    //Win ie以外のXMLHttpRequestオブジェクト実装ブラウザ用
    return new XMLHttpRequest();
  } else {
    return null;
  }
}

//ライブログ記録処理呼び出し
function callLiveLog() {

  var cookieNameId = cookieNameTemplate + ":" + logid;

  ///クッキー読取り/解析
  var cookieParam = GetCookie(cookieNameId);

  //クッキーが存在し、時間が経過している場合
  if (cookieParam != null && Date.parse(cookieParam) < new Date().getTime()) { 
    var path = location.pathname;
      document.cookie = cookieNameId + "=; path=" + location.pathname.substr(0, location.pathname.indexOf("/authdir/")) + "/authdir";
      window.location.href = window.location.href.replace(/\/authdir\/.+/g, "/error.aspx?err=15");
  }
  else if (xmlHttp != null) {

      var totallenParam = "";

      if (typeof (totallen) != "undefined") {

          // 定義済み
          totallenParam = totallen;
      }

      xmlHttp.open("GET", livelogpath + "livelog.aspx?id=" + logid + "&p=" + livelogpage + "&tl=" + totallenParam + "&t=" + (new Date()).getTime() + "&cn=" + cookieNameTemplate, true);
      xmlHttp.onreadystatechange = function() {
      if (xmlHttp.readyState == 4 && xmlHttp.status == 200) {
        var result = xmlHttp.responseText;  //レスポンス結果(logid,タイマー間隔)
        if (result.indexOf(",") != -1) {
          var tmp = result.split(",");
          if (!isNaN(tmp[0])) {
            logid = tmp[0];
          }
          if (logid == -1) {
            //ログ取得終了
            clearInterval(timer);
          }
          else if (!isNaN(tmp[1]) && interval != parseInt(tmp[1])) {
            //タイマー間隔が変更されたので再設定
            interval = parseInt(tmp[1]);
            clearInterval(timer);
            timer = window.setInterval('callLiveLog()', interval);
          }
        }
      }
    }
    xmlHttp.send(null);
  }
}

function GetCookie(name) {

    var cookieName = name + '=';
    var allcookies = document.cookie;
    var position = allcookies.indexOf(cookieName);
    if (position != -1) {
        var startIndex = position + cookieName.length;
        var endIndex = allcookies.indexOf(';', startIndex);
        if (endIndex == -1) endIndex = allcookies.length;
        return decodeURIComponent(allcookies.substring(startIndex, endIndex));
    }

    return "";
}
