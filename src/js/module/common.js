// import $ from 'jquery';
import querystring from 'querystring';


/**
 * 配列と配列の要素が一致するか判別
 */
export const isMatchArray = (a, b) => {
	if (Array.isArray(a) && Array.isArray(b) && a.sort().toString() === b.sort().toString()) {
		return true;
	}
	return false;
}

/**
 * 配列内に値が存在するか判別
 */
export const inArray = (array, value) => {
	if (Array.isArray(array) && array.indexOf(value) >= 0) {
		return true;
	}
	return false;
}

/**
 * URLクエリの取得
 */
export const getUrlQuery = () => {
	return querystring.parse(window.location.search.substring(1));
}

/**
 * 相対URLを絶対URLに変換
 */
export const getAbsUrl = (relativePath) => {
	const a = document.createElement('a');
	a.href = relativePath;
	return a.href;
}

/**
 * EQのAPIリファレンスのパース
 */
export const parseApiKeywords = (apiKeywords) => {
	let obj = {};

	if (apiKeywords) {
		obj = apiKeywords.split(";").reduce((result, query) => {
			let [key, value] = query.split("=");

			if (!result[key])  {
				result[key] = value;
			} else {
				if (Array.isArray(result[key])) {
					result[key].push(value);
				} else {
					result[key] = [result[key], value];
				}
			}

			// tags

			return result;
		}, {});
	}

	return obj;
}