// import $ from 'jquery';
import config from '../config';
import {
	isMatchArray,
	inArray,
	getUrlQuery,
	getAbsUrl,
	parseApiKeywords,
} from './common';
import TacosApi from './tacos-api';


// 二桁変換
Number.prototype.doubleDigits = function () {
	var val = this;
	return val < 10 ? "0" + val : val.toString();
};
String.prototype.doubleDigits = function () {
	return parseInt(this, 10).doubleDigits();
};

class App {
	constructor() {
		// user actionで変化
		this.state = {
			tag: 'all',
			page: {
				current: -1,
				start: 1,
				end: -1,
			},
			videos: [],
			mylist: [],
		};

		// config.jsから定数を取得
		this.mid = typeof window.MOVIEVOD !== 'undefined' && window.MOVIEVOD ? window.MOVIEVOD : '';

		// 一覧のbodyから取得
		this.page = {
			type: null,
			name: null,
		}

		// 全体で使用
		this.xml;
		this.videos = [];
		this.tags = [];

		// xmlのAPIリファレンスキー情報
		this.ics = {};
		this.products = {};

		this.tag = {
			count: -1,
			all: {
				text: '全て',
				key: 'all',
			}
		}

		this.count = {
			video: {
				title: {
					pc: 32,
					sp: 27,
				}
			},
			relatedVideo: {
				title: {
					pc: 22,
					sp: 24,
				}
			},
		}

		this.path =  {
			xml: config.path.xml,
		};

		// id
		this.htmlId = {
			filterTagList: '#filterTagList',
			videoList: '#videoList',
			pagination: '#pagination',
		}
	}

	//-----------------------------
	// Public
	//-----------------------------
	/**
	 * 初期表示（一覧ページ）
	 */
	initList() {
		// ページタイプの取得
		this.getPageType();

		this.getXml('../../' + this.path.xml)
		.then(() => {
			//---------------------------
			// xml parse
			//---------------------------
			// products
			this.getProducts();

			// ics
			this.getIndicatedCodes();

			//---------------------------
			// 要素作成
			//---------------------------
			// フィルタータグ
			this.setFilterTagList();

			// リンクなど作成
			this.setList();

			//---------------------------
			// 一覧作成
			//---------------------------
			// EQ param
			const param = {
				token: config.eq.token,
				pageSize: config.eq.maxCount,
				active_flg: 2,
				api_keywords: 'list=y;dep=' + this.page.type,
				sort1_order: 'publication_date',
				sort1_by: 'desc',
			};

			$.when(
				this.getEqVideos(param),
				this.getMylist(),
			).then((videos, mylist) => {
				// parse EQ data
				this.videos = videos;
	
				// video list
				this.state.page.current = 1;
				this.state.videos = videos;
				this.setVideoList();

				// pagination
				this.setPagination();
			})
			.always(() => {
				// loadingの非表示
				this.hideLoading();
			});
		});
	}

	/**
	 * 初期表示（マイリストページ）
	 */
	initMyList() {
		// ページタイプの取得
		this.getPageType();

		$.when(
			this.getXml('../../' + this.path.xml),
			this.getMylist(),
		)
		.then(() => {
			//---------------------------
			// xml parse
			//---------------------------
			// products
			this.getProducts();

			// ics
			this.getIndicatedCodes();

			//---------------------------
			// 要素作成
			//---------------------------
			// リンクなど作成
			this.setMyList();

			//---------------------------
			// 一覧作成
			//---------------------------
			if (this.state.mylist.length > 0) {
				// EQ param
				const param = {
					token: config.eq.token,
					pageSize: config.eq.maxCount,
					active_flg: 2,
					mid: this.state.mylist.join(','),
					api_keywords: 'list=y',
					sort1_order: 'publication_date',
					sort1_by: 'desc',
				};

				// request EQ
				this.getEqVideos(param)
				.then((videos) => {
					// parse EQ data
					this.videos = videos;
		
					// video list
					this.state.page.current = 1;
					this.state.videos = videos;
					this.setVideoList();

					// pagination
					this.setPagination();
				})
				.always(() => {
					// loadingの非表示
					this.hideLoading();
				});
			} else {
				// loadingの非表示
				this.hideLoading();
			}
		});
	}

	/**
	 * 初期表示（視聴ページ）
	 */
	initPlayer() {
		// ページタイプの取得
		this.getPageType();

		this.getXml('../../../' + this.path.xml)
		.then(() => {
			//---------------------------
			// xml parse
			//---------------------------
			// products
			this.getProducts();

			// ics
			this.getIndicatedCodes();

			//---------------------------
			// EQから対象の動画取得
			//---------------------------
			const param = {
				token: config.eq.token,
				active_flg: 2,
				mid: this.mid,
			};

			//---------------------------
			// プレイヤー作成
			//---------------------------
			$.when(
				this.getEqVideos(param),
				this.getMylist(),
			).then((videos) => {
				if (videos.length > 0) {
					const video = videos[0];
					const eventId = video.apiKeywords.event;

					//---------------------------
					// livelog.jsの読み込み
					//---------------------------
					window.totallen = video.duration;

					const script = document.createElement('script');
					script.src = window.livelogJsPath;
					document.body.appendChild(script);

					//---------------------------
					// 動画情報の出力
					//---------------------------
					this.setPlayer(video);

					//---------------------------
					// 関連動画
					//---------------------------
					// 関連動画のパラメータ作成
					const paramRelated = {
						token: config.eq.token,
						pageSize: config.eq.maxCount,
						active_flg: 2,
					};

					// タグの設定
					let apikeyTags = '';
					video.apiKeywords.tag.forEach((tag, index) => {
						if (apikeyTags === '') {
							apikeyTags += 'tag=' + tag;
						} else {
							apikeyTags += '|tag=' + tag;
						}
					});
					paramRelated.api_keywords = apikeyTags;

					// 関連動画の取得
					this.getEqVideos(paramRelated)
					.then((tagMatchVideos) => {
						if (tagMatchVideos.length > 0) {
							const relatedVideos = tagMatchVideos.filter((tagMatchVideo, index) => {
								// APIリファレンスのlist=yかつmidが一緒ではないものを抽出
								return tagMatchVideo.apiKeywords.list && tagMatchVideo.apiKeywords.list === 'y' && tagMatchVideo.mid != this.mid;
							});

							if (relatedVideos.length > 0) {
								// 関連動画の出力
								this.setRelatedVideoList(relatedVideos, eventId);
							}
						}
					})
					.always(() => {
						// mylist click event
						this.setEventClickMylistButton();
					});
				}
			})
			.always(() => {
				// loadingの非表示
				this.hideLoading();
			});
		});
	}

	//-----------------------------
	// Private
	//-----------------------------
	getXml(url) {
		const dfd = new $.Deferred;

		$.ajax({
			type: 'GET',
			url: url,
			dataType: 'xml',
			cache: false,
			timeout: 10000,
		})
		.then((xml) => {
			this.xml = xml;
			dfd.resolve(xml);
		})
		.catch((xhr, status, error) => {
			console.error('AJAX通信エラー(XML)', xhr, status, error);
			dfd.reject();
		});

		return dfd.promise();
	}

	getEqVideos(param) {
		const dfd = new $.Deferred;

		$.ajax({
			type: 'GET',
			url: config.eq.url,
			dataType: "jsonp",
			data: param,
			cache: false,
			timeout: 30000,
		})
		.then((result) => {
			if (result.response_status === config.eq.status.ok && result.meta.length > 0) {
				let videos = [];

				result.meta.forEach((meta, index) => {
					const title = meta.title.match(/(\||｜)(.*)(\||｜)/);
					const apiKeywords = parseApiKeywords(meta.api_keywords);
					const link = apiKeywords.event ? apiKeywords.event + '/movie/index.html' : '';

					// 必要な動画情報の整理
					const video = {
						// mid: meta.mid,
						mid: meta.mid ? '' + meta.mid : null,
						thumbnail: meta.thumbnail_url_ssl,
						title: title[2] ? title[2] : '',
						duration: meta.duration,
						expirationDate: meta.expiration_date,
						link: link,
						apiKeywords: apiKeywords,
						// sort用
						publicationDate: meta.publication_date,
					}

					videos.push(video);
				});

				dfd.resolve(videos);
			} else {
				dfd.reject();
			}
		})
		.catch((xhr, status, error) => {
			console.error('AJAX通信エラー(EQ)', xhr, status, error);
			dfd.reject();
		});

		return dfd.promise();
	}

	getIndicatedCodes() {
		$(this.xml).find('root > ics > ic').each((index, item) => {
			const $this = $(item);

			const obj = {
				text: $this.attr('name'),
				key: $this.attr('code'),
			};

			this.ics[obj.key] = obj.text;
		});
	}

	getProducts() {
		$(this.xml).find('root > products > product').each((index, item) => {
			const $this = $(item);

			const obj = {
				text: $this.attr('name'),
				key: $this.attr('code'),
			};

			this.products[obj.key] = obj.text;
		});
	}

	getMylist() {
		const dfd = new $.Deferred;

		TacosApi.getBookmark()
		.then((tacos) => {
			if (tacos.result.mid) {
				this.state.mylist = tacos.result.mid;
			}
			dfd.resolve(tacos.result);
		})
		.catch((xhr, status, error) => {
			dfd.reject();
		});

		return dfd.promise();
	}
	
	setFilterTagList() {
		let html = '';
		html += this.getHtmlFilterTagListItem({
			text: this.tag.all.text,
			key: this.tag.all.key,
			isSelected: true,
		});

		let count = 0;

		$(this.xml).find('root > tags' + '[dep=' + this.page.type + '] > tag').each((index, item) => {
			const $this = $(item);

			const tag = {
				text: $this.attr('name'),
				key: $this.attr('code'),
			};

			// タグの保存
			if (!inArray(this.tags, tag.key)) {
				this.tags.push(tag.key);
			}

			html += this.getHtmlFilterTagListItem(tag);
			count++;
		});

		$('#filterTagList').html(html);

		// フィルタータグの開閉イベント
		$('#filterTagHeader').click(e => {
			const $this = $(e.currentTarget);

			$('#filterTag').toggleClass('is-active');
		});

		// フィルタータグのクリックイベント
		$('.p-filterTagList__item').click(e => {
			const $this = $(e.currentTarget);
			const key = $this.data('tag-filter');
			const $prev = $('[data-tag-filter="' + this.state.tag + '"]');

			// update
			this.state.tag = key;
			$prev.removeClass('is-selected');
			$this.addClass('is-selected');

			// filter
			this.filterVideoList();

			return false;
		});
	}

	setVideoList() {
		let html = '';

		// page
		const startIndex = config.displayCount.videoList.max * (this.state.page.current - 1);
		const endIndex = startIndex + config.displayCount.videoList.max;

		const videos = this.state.videos.filter((video, index) => {
			return index >= startIndex && index < endIndex;
		});

		videos.forEach((video, index) => {
			html += this.getHtmlVideoItem(video);
		});

		$('#videoList').html(html);

		// mylist button event
		this.setEventClickMylistButton();
	}

	setPagination() {
		this.state.page.end = this.state.videos.length < config.displayCount.videoList.max ? 1 : Math.ceil(this.state.videos.length / config.displayCount.videoList.max);

		$('#pagination').html(this.getHtmlPagination());

		$('[data-page]').click((e) => {
			const $this = $(e.currentTarget);
			const page = $this.data('page');

			// css更新
			$('[data-page]').removeClass('is-current');
			$this.addClass('is-current');

			// 一覧の更新
			this.state.page.current = page;
			this.setVideoList();

			// ページトップへ移動
			$('body, html').animate({ scrollTop: 0 }, 500);

			return false;
		});
	}

	setList() {
		// mylist link
		const $mylist = $('#mylistLink');
		const mylistLink = $mylist.attr('href');
		$mylist.attr('href', mylistLink + '?dep=' + this.page.type);
	}

	setMyList() {
		const query = getUrlQuery();

		// mylist link
		const $mylist = $('#mylistLink');
		const mylistLink = $mylist.attr('href');
		$mylist.attr('href', mylistLink + '?dep=' + query.dep);

		// list link
		$('#listLink').attr('href', '../' + query.dep + '_list/index.html');
	}

	setPlayer(video) {
		// // ページタイトル
		// document.title = video.title;

		// header link
		const listLink = getAbsUrl('../../' + video.apiKeywords.dep + '_list/index.html');
		$('#listLink').attr('href', listLink);

		// mylist link
		const $mylist = $('#mylistLink');
		const mylistLink = $mylist.attr('href');
		$mylist.attr('href', mylistLink + '?dep=' + video.apiKeywords.dep);

		// related video link
		$('#relatedVideoLink').attr('href', listLink);

		// // title
		// $('#videoTitle').html(video.title);

		// mid
		$('#eqPlayer').attr('data-normal-speed-mid', video.mid);

		// info
		$('#videoTime').html(this.getTextVideoTime(video.duration));
		if (video.expirationDate) {
			$('#videoDate').html(this.getTextVideoExpiredDate(video.expirationDate));
		} else {
			$('#videoExpiration').hide();
			$('#videoDate').hide();
		}

		// label
		const productLabel = video.apiKeywords.product && this.products[video.apiKeywords.product] ? this.products[video.apiKeywords.product] : '';
		const icLabel = video.apiKeywords.ic && this.ics[video.apiKeywords.ic] ? this.ics[video.apiKeywords.ic] : '';

		const labels = [
			productLabel != '' ? '<li class="p-videoLabelList__item p-videoLabelList__item--' + video.apiKeywords.product + '">' + productLabel + '</li>' : '',
			icLabel != '' ? '<li class="p-videoLabelList__item">' + icLabel + '</li>' : '',
		].join('');

		$('#videoLabelList').html(labels);

		// mylist
		$('#checkButton').attr('data-mylist-mid', video.mid);
		$('#checkButton').attr('data-mylist-eid', video.apiKeywords.event);
	}

	setRelatedVideoList(videos, eventId) {
		let html = '';

		videos.forEach((video, index) => {
			html += this.getHtmlRelatedVideoItem(video, eventId);
		});

		// 関連動画一覧
		$('#relatedVideoList').html(html);

		// カルーセル制御
		const options = {
			autoplay: false,
			infinite: false,
			dots: false,
			arrows: true,
			prevArrow: '<div class="slick-arrow-custom slick-prev-custom"></div>',
			nextArrow: '<div class="slick-arrow-custom slick-next-custom"></div>',
			slidesToShow: config.displayCount.relatedVideoList.pc,
			slidesToScroll: config.displayCount.relatedVideoList.pc,
			responsive: [{
				breakpoint: config.breakPoint,
				settings: {
					slidesToShow: config.displayCount.relatedVideoList.sp,
					slidesToScroll: config.displayCount.relatedVideoList.sp,
				}
			}]
		};
		$('#relatedVideoList').slick(options);

		// 表示
		$('#relatedVideoWrapper').show();
	}

	setEventClickMylistButton() {
		$('[data-mylist-mid]').click((e) => {
			const $this = $(e.currentTarget);

			const mid = $this.data('mylist-mid') ? '' + $this.data('mylist-mid') : null;
			const eid = $this.data('mylist-eid') ? '' + $this.data('mylist-eid') : null;
			const isActive = $this.hasClass('is-active');

			if (isActive) {
				// 削除
				const param = {
					mid: mid,
					type: TacosApi.bookmark.delete.type,
				};
				
				// request delete
				TacosApi.setBookmark(param)
				.then(() => {
					$this.removeClass('is-active');

					// mylist update
					if (inArray(this.state.mylist, mid)) {
						this.state.mylist = this.state.mylist.filter(mylistId => mylistId !== mid);
					}
				});

				// live log
				const logParam = {
					id: 0,
					p: this.page.name,
					eventid: eid,
					op5: TacosApi.bookmark.delete.text + eid,
				}

				TacosApi.setLiveLog(logParam);
			} else {
				// 登録
				const param = {
					mid: mid,
					type: TacosApi.bookmark.add.type,
				};

				// request add
				TacosApi.setBookmark(param)
				.then(() => {
					$this.addClass('is-active');

					// mylist update
					if (!inArray(this.state.mylist, mid)) {
						this.state.mylist.push(mid);
					}
				});

				// live log
				const logParam = {
					id: 0,
					p: this.page.name,
					eventid: eid,
					op5: TacosApi.bookmark.add.text + eid,
				}

				TacosApi.setLiveLog(logParam);
			}
		});
	}

	filterVideoList() {
		// tag filter
		if (this.state.tag == this.tag.all.key) {
			// all
			this.state.videos = this.videos;
		} else {
			this.state.videos = this.videos.filter((video, index) => {
				return video.apiKeywords.tag.some(tag => tag == this.state.tag);
			});
		}

		this.state.page.current = 1;

		this.setVideoList();
		this.setPagination();
	}

	getPageType() {
		this.page.type = $('body').data('page-type');

		if (this.page.type === 'movie') {
			this.page.name = this.page.type;
		} else {
			this.page.name = 'list_' + this.page.type;
		}
	}

	getHtmlFilterTagListItem(tag) {
		const selected = tag.isSelected ? ' is-selected' : ''; 
		const html = '<li class="p-filterTagList__item' + selected + '" data-tag-filter="' + tag.key + '">' + tag.text + '</li>';
		return html;
	}

	// const video = {
	// 	mid: meta.mid,
	// 	thumbnail: meta.thumbnail_url_ssl,
	// 	title: title[2],
	// 	duration: meta.duration,
	// 	expirationDate: meta.expiration_date,
	// 	link: link,
	// 	apiKeywords: apiKeywords,
	// }
	getHtmlVideoItem(video) {
		const productLabel = video.apiKeywords.product && this.products[video.apiKeywords.product] ? this.products[video.apiKeywords.product] : '';
		const icLabel = video.apiKeywords.ic && this.ics[video.apiKeywords.ic] ? this.ics[video.apiKeywords.ic] : '';

		const isNew = this.isVideoNew(video.publicationDate) ? ' is-new' : '';
		const isLongVideo = video.duration >= 3600 ? ' is-longVideo' : '';

		const isCheckedMylist = inArray(this.state.mylist, video.mid) ? ' is-active' : '';

		const html = [
			'<li class="p-videoList__item">',
			'<div class="p-videoItem">',
			'<a class="p-videoItem__link" href="' + '../' + video.link + '?ref=' + this.page.name + '">',
				'<div class="p-videoItem__thumb' + isNew + '"><img src="' + video.thumbnail + '" alt=""></div>',
				'<p class="p-videoItem__title u-spNone">' + this.getTextTitleWithDots(video.title, this.count.video.title.pc) + '</p>',
				'<p class="p-videoItem__title u-pcNone">' + this.getTextTitleWithDots(video.title, this.count.video.title.sp) + '</p>',
			'</a>',
			'<div class="p-videoItem__detail">',
				'<div class="p-videoItem__info">',
					'<div class="p-videoInfo' + isLongVideo + '">',
						'<div class="p-videoInfo__time">' + this.getTextVideoTime(video.duration) + '</div>',
						video.expirationDate ? '<div class="p-videoInfo__expired">視聴期限</div>' : '',
						video.expirationDate ? '<div class="p-videoInfo__date">' + this.getTextVideoExpiredDate(video.expirationDate) + '</div>': '',
					'</div>',
				'</div>',
				'<div class="p-videoItem__labelList">',
					'<ul class="p-videoLabelList">',
						productLabel !== '' ? '<li class="p-videoLabelList__item p-videoLabelList__item--' + video.apiKeywords.product + '">' + productLabel + '</li>' : '',
						icLabel !== '' ? '<li class="p-videoLabelList__item">' + icLabel + '</li>' : '',
					'</ul>',
				'</div>',
				'<div class="p-videoItem__checkButton' + isCheckedMylist + '" data-mylist-mid="' + video.mid + '" data-mylist-eid="' + video.apiKeywords.event + '"></div>',
			'</div>',
			'</li>'
		].join('');

		return html;
	}

	getHtmlPagination() {
		let html = '';

		// 1ページのときは非表示
		if (this.state.page.end <= 1) {
			return html;
		}

		for (let count = 1; count <= this.state.page.end; count++) {
			const isCurrent = count === this.state.page.current ? ' is-current' : '';

			html += [
				'<li class="p-pagination__item">',
				'<a class="p-pagination__link' + isCurrent + '" data-page="' + count + '" href="">' + count + '</a>',
				'</li>',
			].join('');
		}

		return html;
	}

	getHtmlRelatedVideoItem(video, eventId) {
		const productLabel = video.apiKeywords.product && this.products[video.apiKeywords.product] ? this.products[video.apiKeywords.product] : '';
		const icLabel = video.apiKeywords.ic && this.ics[video.apiKeywords.ic] ? this.ics[video.apiKeywords.ic] : '';
		
		const isNew = this.isVideoNew(video.publicationDate) ? ' is-new' : '';

		const isCheckedMylist = inArray(this.state.mylist, video.mid) ? ' is-active' : '';

		const html = [
			'<li class="p-relatedVideoList__item">',
				'<div class="p-relatedVideoItem">',
					'<a class="p-relatedVideoItem__link" href="' + '../../' + video.link + '?ref=' + eventId + '_related' + '">',
						'<div class="p-relatedVideoItem__thumb' + isNew +'">',
							'<img src="' + video.thumbnail + '" alt="">',
						'</div>',
						'<p class="p-relatedVideoItem__title u-spNone">' + this.getTextTitleWithDots(video.title, this.count.relatedVideo.title.pc) + '</p>',
						'<p class="p-relatedVideoItem__title u-pcNone">' + this.getTextTitleWithDots(video.title, this.count.relatedVideo.title.sp) + '</p>',
					'</a>',
					'<div class="p-relatedVideoItem__detail">',
						'<div class="p-relatedVideoItem__info">',
							'<div class="p-relatedVideoInfo">',
								'<div class="p-relatedVideoInfo__time">' + this.getTextVideoTime(video.duration) + '</div>',
								video.expirationDate ? '<div class="p-relatedVideoInfo__expired">視聴期限</div>' : '',
								video.expirationDate ? '<div class="p-relatedVideoInfo__date">' + this.getTextVideoExpiredDate(video.expirationDate) + '</div>' : '',
							'</div>',
						'</div>',
						'<div class="p-relatedVideoItem__labelList">',
							'<ul class="p-relatedVideoLabelList">',
								productLabel !== '' ? '<li class="p-relatedVideoLabelList__item p-relatedVideoLabelList__item--' + video.apiKeywords.product + '">' + productLabel + '</li>' : '',
								icLabel !== '' ? '<li class="p-relatedVideoLabelList__item">' + icLabel + '</li>' : '',
							'</ul>',
						'</div>',
						'<div class="p-relatedVideoItem__checkButton' + isCheckedMylist + '" data-mylist-mid="' + video.mid + '" data-mylist-eid="' + video.apiKeywords.event + '"></div>',
					'</div>',
				'</div>',
			'</li>',
		].join('');

		return html;
	}

	getTextVideoTime(time) {
		let text = '';

		const hour = Math.floor(time / 60 / 60);
		const min = Math.floor((time / 60) % 60);
		const sec = time % 60;

		if (hour > 0) {
			text = hour.doubleDigits() + '時間' + min.doubleDigits() + '分' + sec.doubleDigits() + '秒';
		} else {
			text = min.doubleDigits() + '分' + sec.doubleDigits() + '秒';
		}

		return  text;
	}

	getTextVideoExpiredDate(value) {
		const date = new Date(value);
		return date.getFullYear() + '年' + (date.getMonth() + 1) + '月' + date.getDate() + '日まで';
	}

	getTextTitleWithDots(title, count) {
		let result = '';

		const maxLength = {
			normal: count * 2,
			small: Math.round(count * 2 * 0.78),
		}

		let length = 0;
		let halfCount = 0;

		for(let i = 0; i < title.length; i++) {
			const c = title.charCodeAt(i);
			if ((c >= 0x0 && c < 0x81) || (c === 0xf8f0) || (c >= 0xff61 && c < 0xffa0) || (c >= 0xf8f1 && c < 0xf8f4)) {
				length += 1;
				halfCount++;
			} else {
				length += 2;
			}

			const tmp = title.substr(0, i + 1);

			if (length >= maxLength.normal || halfCount >= maxLength.small) {
				result = result.slice(0, -1) + '...';
				break;
			} else {
				result = tmp;
			}
		}

		return result;
	}

	isVideoNew(date) {
		if (!date) {
			return false;
		}

		const target = new Date(date);
		const threshold = new Date();

		threshold.setDate(threshold.getDate() - 30);
		threshold.setHours(0);
		threshold.setMinutes(0);
		threshold.setSeconds(0);

		if (target.getTime() >= threshold.getTime()) {
			return true;
		}

		return false;
	}

	hideLoading() {
		// loadingの非表示
		$('.u-loading').fadeOut('slow');
	}
}

export default new App();